{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "\n",
    "# Crystal Demo [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/crystal.ipynb\n",
    "\n",
    "This is a demo that demonstrates crystallisation on the surface of a liquid due to cooling. See\n",
    "http://www.ctcms.nist.gov/fipy/examples/phase/generated/examples.phase.anisotropy.html for more details."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before anything else let us set up the grid and the function space. We use the default DoF storage available in ```dune-fem``` - this can be changed for example to ```istl,eigen``` or ```petsc```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import dune.fem as fem\n",
    "from dune.grid import Marker, cartesianDomain\n",
    "import dune.create as create\n",
    "order = 1\n",
    "dimDomain = 2     # we are solving this in 2d\n",
    "dimRange = 2      # we have a system with two unknowns\n",
    "domain = cartesianDomain([4, 4], [8, 8], [3, 3])\n",
    "grid   = create.view(\"adaptive\", grid=\"ALUConform\",\n",
    "                    constructor=domain, dimgrid=dimDomain)\n",
    "space  = create.space(\"lagrange\", grid, dimrange=dimRange, \n",
    "                order=order, storage=\"fem\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We want to solve the following system of equations of variables $\\phi$ (phase field) and $T$ (temperature field)\n",
    "\n",
    "\\begin{gather}\n",
    "\\tau \\frac{\\partial \\phi}{\\partial t} = \\nabla \\cdot D \\nabla \\phi + \\phi(1-\\phi)m(\\phi, T), \\\\\n",
    "\\frac{\\partial T}{\\partial t} = D_T \\nabla^2 T + \\frac{\\partial \\phi}{\\partial t},\n",
    "\\end{gather}\n",
    "\n",
    "where $D_T$ = 2.25, m is given by\n",
    "\n",
    "\\begin{equation}\n",
    "m(\\phi, T) = \\phi - \\frac{1}{2} - \\frac{\\kappa_1}{\\pi} \\arctan(\\kappa_2 T),\n",
    "\\end{equation}\n",
    "\n",
    "D is given by\n",
    "\n",
    "\\begin{equation}\n",
    "D = \\alpha^2(1+c\\beta)\\left(\\begin{array}{cc}\n",
    "1 + c\\beta & -c \\frac{\\partial \\beta}{\\partial \\psi} \\\\\n",
    "c \\frac{\\partial \\beta}{\\partial \\psi} & 1+c\\beta\n",
    "\\end{array}\\right),\n",
    "\\end{equation}\n",
    "\n",
    "$\\beta = \\frac{1-\\Phi^2}{1+\\Phi^2}$, $\\Phi = \\tan \\left( \\frac{N}{2} \\psi \\right)$, $\\psi = \\theta + \\arctan \\left(\\frac{\\partial \\phi/ \\partial y}{\\partial \\phi / \\partial x} \\right)$ and $\\theta$, $N$ are constants.\n",
    "\n",
    "Let us first set up the parameters for the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "alpha        = 0.015\n",
    "tau          = 3.e-4\n",
    "kappa1       = 0.9\n",
    "kappa2       = 20.\n",
    "c            = 0.02\n",
    "N            = 6."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We define the initial data and create a function from it. We use this value to set up our solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "def initial(x):\n",
    "    r  = (x - [6, 6]).two_norm\n",
    "    return [ 0 if r > 0.3 else 1, -0.5 ]\n",
    "initial_gf = create.function(\"global\", grid, \"initial\", order+1, initial)\n",
    "solution   = space.interpolate(initial_gf, name=\"solution\")\n",
    "solution_n = solution.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "As we will be discretising in time, we define the unknown data as $u = (\\phi_1, \\Delta T_1)$, while given data (from the previous time step) is $u_n = (\\phi_0, \\Delta T_0)$ and test function $v = (v_0, v_1)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from ufl import TestFunction, TrialFunction, Coefficient, Constant, triangle\n",
    "u = TrialFunction(space)\n",
    "v = TestFunction(space)\n",
    "un = Coefficient(space)   # coefficient used in ufl for old solution\n",
    "dt = Constant(triangle)      # set to time step size later on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "For the numerical scheme, we discretise the time derivatives in the usual way, and we obtain the weak form by multiplying by a test function and integrating by parts. We also express the system using vectors.\n",
    "\n",
    "This gets us the following equation.\n",
    "\n",
    "\\begin{equation}\n",
    "\\int \\left( \\alpha^2 \\frac{dt}{\\tau} (D_n\\nabla \\phi_1) \\cdot \\nabla v_0 + dt \\ D_T \\nabla T_1 \\cdot \\nabla v_1 + \\textbf{u} \\cdot \\textbf{v} - \\textbf{s} \\cdot \\textbf{v} \\right) \\ dx \n",
    "=\n",
    "\\int (\\textbf{u}_n \\cdot \\textbf{v} - \\phi_0 v_1) \\ dx \n",
    "\\end{equation}\n",
    "\n",
    "where\n",
    "\n",
    "\\begin{equation}\n",
    "\\textbf{s} = \\left( \\frac{dt}{\\tau}\\phi_1(1-\\phi_1)m(\\phi_1, T_1), \\phi_1 \\right)^T\n",
    "\\end{equation}\n",
    "\n",
    "and\n",
    "\n",
    "$D_n$ is the anisotropic diffusion using the previous solution $\\textbf{u}_n$ to compute the entries.\n",
    "\n",
    "First we put in the right hand side which only contains explicit data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from ufl import inner, dx\n",
    "a_ex = (inner(un, v) - inner(un[0], v[1])) * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "For the left hand side we have the spatial derivatives and the implicit parts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from ufl import pi, atan, atan_2, tan, grad, as_vector, inner, dot\n",
    "psi        = pi/8.0 + atan_2(grad(un[0])[1], (grad(un[0])[0]))\n",
    "Phi        = tan(N / 2.0 * psi)\n",
    "beta       = (1.0 - Phi*Phi) / (1.0 + Phi*Phi)\n",
    "dbeta_dPhi = -2.0 * N * Phi / (1.0 + Phi*Phi)\n",
    "fac        = 1.0 + c * beta\n",
    "diag       = fac * fac\n",
    "offdiag    = -fac * c * dbeta_dPhi\n",
    "d0         = as_vector([diag, offdiag])\n",
    "d1         = as_vector([-offdiag, diag])\n",
    "m          = u[0] - 0.5 - kappa1 / pi*atan(kappa2*u[1])\n",
    "s          = as_vector([dt / tau * u[0] * (1.0 - u[0]) * m, u[0]])\n",
    "a_im = (alpha*alpha*dt / tau * (inner(dot(d0, grad(u[0])), grad(v[0])[0]) +\n",
    "                                inner(dot(d1, grad(u[0])), grad(v[0])[1]))\n",
    "       + 2.25 * dt * inner(grad(u[1]), grad(v[1])) \n",
    "       + inner(u,v) - inner(s,v)) * dx\n",
    "\n",
    "equation = a_im == a_ex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We set up the model and the scheme with some parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "model  = create.model(\"integrands\", grid, equation, coefficients={un:solution_n} )\n",
    "solverParameters = {\n",
    "        \"fem.solver.newton.tolerance\": 1e-5,\n",
    "        \"fem.solver.newton.linabstol\": 1e-8,\n",
    "        \"fem.solver.newton.linreduction\": 1e-8,\n",
    "        \"fem.solver.newton.verbose\": 0,\n",
    "        \"fem.solver.newton.linear.verbose\": 0\n",
    "    }\n",
    "scheme = create.scheme(\"galerkin\", model, space, solver=\"gmres\",\n",
    "        parameters=solverParameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We set up the adaptive method. We start with a marking strategy based on the value of the gradient of the phase field variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "def mark(element):\n",
    "    solutionLocal = solution.localFunction(element)\n",
    "    grad = solutionLocal.jacobian(element.geometry.referenceElement.center)\n",
    "    if grad[0].infinity_norm > 1.2:\n",
    "        return Marker.refine if element.level < maxLevel else Marker.keep\n",
    "    else:\n",
    "        return Marker.coarsen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We do the initial refinement of the grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "maxLevel = 11\n",
    "hgrid    = grid.hierarchicalGrid\n",
    "hgrid.globalRefine(6)\n",
    "for i in range(0,maxLevel):\n",
    "    hgrid.mark(mark)\n",
    "    fem.adapt(hgrid,[solution])\n",
    "    fem.loadBalance(hgrid,[solution])\n",
    "    solution.interpolate(initial_gf)\n",
    "    print(grid.size(0),end=\" \")\n",
    "print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Let us start by plotting the initial state of the material, which is just a small circle in the centre."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from dune.fem.plotting import plotComponents as plotComponents\n",
    "import matplotlib.pyplot as pyplot\n",
    "from dune.fem.function import levelFunction, partitionFunction\n",
    "import matplotlib\n",
    "vtk = grid.sequencedVTK(\"crystal\", pointdata=[solution],\n",
    "       celldata=[levelFunction(grid), partitionFunction(grid)])\n",
    "\n",
    "matplotlib.rcParams.update({'font.size': 10})\n",
    "matplotlib.rcParams['figure.figsize'] = [10, 5]\n",
    "plotComponents(solution, cmap=pyplot.cm.rainbow, show=[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Some constants needed for the time loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "timeStep     = 0.0005\n",
    "model.setConstant(dt, timeStep)\n",
    "t        = 0.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Finally we set up the time loop and solve the problem - each time this cell is run the simulation will progress to the given ```endTime``` and then the result is shown. Just rerun it multiple times while increasing the ```endTime``` to progress the simulation further - this might take a bit..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "endTime = 0.05\n",
    "while t < endTime:\n",
    "    solution_n.assign(solution)\n",
    "    scheme.solve(target=solution)\n",
    "    print(t,grid.size(0),end=\"\\r\")\n",
    "    t += timeStep\n",
    "    hgrid.mark(mark)\n",
    "    fem.adapt(hgrid,[solution])\n",
    "    fem.loadBalance(hgrid,[solution])\n",
    "    # vtk()\n",
    "print()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plotComponents(solution, cmap=pyplot.cm.rainbow)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
