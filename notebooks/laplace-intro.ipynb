{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Solving an Elliptic PDE [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/laplace-intro.ipynb\n",
    "\n",
    "This demo introduces basic usage of dune-fempy, using the Poisson equation as an example. Namely,\n",
    "\n",
    "\\begin{align*}\n",
    "  - \\Delta u + u &= f && \\text{in } \\Omega \\\\\n",
    "  \\nabla u \\cdot \\textbf{n} &= 0 && \\text{on } \\Gamma\n",
    "\\end{align*}\n",
    "\n",
    "\n",
    "If you have compiled DUNE against MPI, we strongly advise you to first initialize MPI from Python.\n",
    "At least OpenMPI is known to fail, if initialized only in the dune-fempy library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass\n",
    "import dune.fem\n",
    "from dune.fem.plotting import plotPointData as plot\n",
    "dune.fem.parameter.append(\"parameter\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "First, we create our computational grid. Our domain will be the unit square divided into 8x8 quadrilaterals. To actually create the grid, we choose an implementation of the DUNE grid interface: a 2-dimensional ALUGrid with simplices and conforming bisection refinement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "import dune.create as create\n",
    "grid = create.grid(\"ALUConform\", dune.grid.cartesianDomain([0, 0], [1, 1], [4, 4]), dimgrid=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We set up the base variables u, v and x in UFL."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from dune.ufl import Space\n",
    "from ufl import TestFunction, TrialFunction, SpatialCoordinate\n",
    "uflSpace = Space((grid.dimGrid, grid.dimWorld), 1)\n",
    "u = TrialFunction(uflSpace)\n",
    "v = TestFunction(uflSpace)\n",
    "x = SpatialCoordinate(uflSpace.cell())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Next we define the equation for the weak form, given by\n",
    "\n",
    "\\begin{equation}\n",
    "\\int_{\\Omega} uv + \\nabla u\\cdot\\nabla v \\ dx =  \\int_{\\Omega} f v \\ dx.\n",
    "\\end{equation}\n",
    "We take $f = 8\\pi^2\\cos(2\\pi x_0)\\cos(2\\pi x_1)$.\n",
    "\n",
    "Note that then the exact solution is then\n",
    "$u = \\cos(2\\pi x_0)\\cos(2\\pi x_1)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from math import pi,log10\n",
    "from ufl import cos, as_vector, dx, grad, inner\n",
    "f = (8*pi*pi+1)*cos(2*pi*x[0])*cos(2*pi*x[1])\n",
    "exact = as_vector( [cos(2.*pi*x[0])*cos(2.*pi*x[1])] )\n",
    "equation = (inner(grad(u), grad(v)) + inner(u,v)) * dx == f * v[0] * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We create the space and the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "spc = create.space(\"lagrange\", grid, dimrange=1, order=1)\n",
    "model = create.model(\"integrands\", grid, equation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We create the scheme and set parameters for the solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "scheme = create.scheme(\"galerkin\", model, spc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We create a grid function for our exact solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "exact_gf = create.function(\"ufl\", grid, \"exact\", 5, exact)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now we solve the system. We assign the solution to `uh`, and define a function to calculate the $L^2$ error, i.e. $|u_h - u|_{L^2}$. We output the data to a vtk file with name `laplace`, and plot it using `plot`. Finally we refine the grid twice and repeat the process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from math import sqrt\n",
    "levels=2\n",
    "for i in range(levels):\n",
    "    print(\"solve on level\", i, \"number of dofs=\", spc.size)\n",
    "    uh,_ = scheme.solve()\n",
    "    def l2error(en,x):\n",
    "        val = uh.localFunction(en).evaluate(x) - exact_gf.localFunction(en).evaluate(x)\n",
    "        return [ val[0]*val[0] ];\n",
    "    l2error_gf = create.function(\"local\", grid, \"error\", 5, l2error)\n",
    "    error = sqrt(l2error_gf.integrate()[0])\n",
    "\n",
    "    print(\"size:\", grid.size(0), \"L2-error:\", error)\n",
    "    grid.writeVTK(\"laplace\", pointdata=[uh, l2error_gf])\n",
    "    plot(uh,gridLines=\"black\")\n",
    "\n",
    "    if i < levels-1:\n",
    "        grid.hierarchicalGrid.globalRefine(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Congratulations! You have successfully solved and visualized your first PDE using dune-fempy.\n",
    "\n",
    "But it is still rather coarse... so either we refine the grid a bit further or we use a second order method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "spc = create.space(\"lagrange\", grid, dimrange=1, order=2)\n",
    "# create the scheme but change some of the default parameters..\n",
    "scheme = create.scheme(\"galerkin\", model, spc, \\\n",
    "       parameters=\\\n",
    "       {\"fem.solver.newton.tolerance\": 1e-9,\n",
    "        \"fem.solver.newton.linabstol\": 1e-12,\n",
    "        \"fem.solver.newton.linreduction\": 1e-12,\n",
    "        \"fem.solver.newton.verbose\": 1,\n",
    "        \"fem.solver.newton.linear.verbose\": 0})\n",
    "\n",
    "print(\"solve with second order\", i, \"number of dofs=\", spc.size)\n",
    "\n",
    "uh,info = scheme.solve()\n",
    "def l2error(en,x):\n",
    "    val = uh.localFunction(en).evaluate(x) - exact_gf.localFunction(en).evaluate(x)\n",
    "    return [ val[0]*val[0] ];\n",
    "l2error_gf = create.function(\"local\", grid, \"error\", 5, l2error)\n",
    "\n",
    "error = sqrt(l2error_gf.integrate()[0])\n",
    "print(\"size:\", grid.size(0), \"L2-error:\", error)\n",
    "plot(uh,level=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Thats looks better already.\n",
    "\n",
    "Plot the log differecence between the solution and the exact solution - also retrieve the information returned by the solver:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "def error(en,x):\n",
    "    val = uh.localFunction(en).evaluate(x) - exact_gf.localFunction(en).evaluate(x)\n",
    "    return [ log10(abs(val[0])) ];\n",
    "error_gf = create.function(\"local\", grid, \"error\", 5, error)\n",
    "\n",
    "plot(error_gf, level=5,clim=[-5,-2])\n",
    "print(\"Solver information: \", info)"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 0
}
