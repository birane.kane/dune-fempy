{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mean Curvature Flow [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/mcf.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example we compute the mean curvature flow of a surface:\n",
    "\\begin{align}\n",
    "   \\frac{\\partial}{\\partial_t} x &= H(x)  && \\text{for} x\\in\\Gamma(t)\n",
    "\\end{align}\n",
    "Assume we can define a reference surface $\\Gamma_0$ such that\n",
    "we can write the evolving surface $\\Gamma(t)$ in the form\n",
    "\\begin{gather}\n",
    "  \\Gamma(t) = X(t,\\Gamma_0)\n",
    "\\end{gather}\n",
    "It is now possible to show that the vector valued function $X=X(t,x)$ with $x\\in\\Gamma_0$ satisfies:\n",
    "\\begin{gather}\n",
    "  \\frac{\\partial}{\\partial_t}X = - H(X)\\nu(X)\n",
    "\\end{gather}\n",
    "where $H$ is the mean curvature of $\\Gamma_t$ and $\\nu$ is its outward pointing normal.\n",
    "\n",
    "We will solve this using a finite element approach based on the following time discrete approximation:\n",
    "\\begin{gather}\n",
    "  \\int_{\\Gamma^n} \\big( U^{n+1} - {\\rm id}\\big) \\cdot \\varphi +\n",
    "    \\tau \\int_{\\Gamma^n} \\big(\n",
    "    \\theta\\nabla_{\\Gamma^n} U^{n+1} + (1-\\theta) I \\big)\n",
    "    \\colon\\nabla_{\\Gamma^n}\\varphi\n",
    "  =0~.\n",
    "\\end{gather}\n",
    "Here $U^n$ parametrizes $\\Gamma(t^{n+1})$ over\n",
    "$\\Gamma^n:=\\Gamma(t^{n})$,\n",
    "$I$ is the identity matrix, $\\tau$ is the time step and\n",
    "$\\theta\\in[0,1]$ is a discretization parameter.\n",
    "<img src=\"mcf.gif\" style=\"height:228px;\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass\n",
    "\n",
    "import math\n",
    "\n",
    "import ufl\n",
    "import dune.ufl\n",
    "import dune.create as create\n",
    "import dune.geometry as geometry\n",
    "import dune.fem as fem\n",
    "from dune.fem.plotting import plotPointData as plot\n",
    "import matplotlib.pyplot as pyplot\n",
    "from IPython import display\n",
    "\n",
    "# polynomial order of surface approximation\n",
    "order = 2\n",
    "\n",
    "# initial radius\n",
    "R0 = 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# set up reference domain Gamma_0\n",
    "grid = create.grid(\"ALUConform\", \"sphere.dgf\", dimgrid=2, dimworld=3)\n",
    "# grid.hierarchicalGrid.globalRefine(1)\n",
    "\n",
    "# space on Gamma_0 to describe position of Gamma(t)\n",
    "spc = create.space(\"lagrange\", grid, dimrange=grid.dimWorld, order=order)\n",
    "# non spherical initial surgface\n",
    "positions = spc.interpolate(lambda x: x * \n",
    "                      (1.+0.5*math.sin(2.*math.pi*x[0]*x[1])*\n",
    "                            math.cos(math.pi*x[2])),\n",
    "                      name=\"position\")\n",
    "\n",
    "# space for discrete solution on Gamma(t)\n",
    "surface   = create.view(\"geometry\",positions)\n",
    "spc = create.space(\"lagrange\", surface, dimrange=surface.dimWorld, order=order)\n",
    "solution  = spc.interpolate(lambda x: x, name=\"solution\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    from mayavi import mlab\n",
    "    from mayavi.tools.notebook import display as display3d\n",
    "    import numpy\n",
    "    mlab.init_notebook()\n",
    "    def show3d():\n",
    "        t = surface.tesselate(level=2)\n",
    "        x = t[0][:,0]\n",
    "        y = t[0][:,1]\n",
    "        z = t[0][:,2]\n",
    "        v = solution.pointData(level=2)\n",
    "        v = numpy.sum(numpy.abs(v)**2,axis=1)**(1./2)\n",
    "        mlab.figure(bgcolor = (1,1,1))\n",
    "        s = mlab.triangular_mesh(x,y,z, t[1], scalars=v)\n",
    "        display3d(s)    \n",
    "except ImportError:\n",
    "    def show3d():\n",
    "        pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# set up model using theta scheme\n",
    "theta = 0.5   # Crank-Nicholson\n",
    "\n",
    "u = ufl.TrialFunction(spc)\n",
    "v = ufl.TestFunction(spc)\n",
    "x = ufl.SpatialCoordinate(spc.cell())\n",
    "I = ufl.Identity(3)\n",
    "tau = ufl.Constant(spc.cell())\n",
    "\n",
    "a = (ufl.inner(u - x, v) + tau * ufl.inner(\n",
    "          theta*ufl.grad(u) + (1-theta)*I, ufl.grad(v)\n",
    "    )) * ufl.dx\n",
    "model = create.model(\"elliptic\", surface, a == 0)\n",
    "\n",
    "scheme = create.scheme(\"h1\", model, spc, solver=\"cg\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "count   = 0\n",
    "t       = 0.\n",
    "endTime = 0.1\n",
    "dt      = 0.005\n",
    "model.setConstant(tau,dt)\n",
    "\n",
    "fig = pyplot.figure(figsize=(15,15))\n",
    "plot(solution, figure=(fig, 131), colorbar=False, gridLines=\"\", triplot=True)\n",
    "show3d()\n",
    "\n",
    "while t < endTime:\n",
    "    scheme.solve(target=solution)\n",
    "    t     += dt\n",
    "    count += 1\n",
    "    positions.dofVector.assign(solution.dofVector)\n",
    "    if count % 10 == 0:\n",
    "        # surface.writeVTK(\"mcf\"+str(order)+\"-0-\", pointdata=[solution], number=count)\n",
    "        # surface.writeVTK(\"mcf\"+str(order)+\"-3-\", pointdata=[solution], number=count, subsampling=3)\n",
    "        plot(solution, figure=(fig, 131+count/10), colorbar=False, gridLines=\"\", triplot=True)\n",
    "        show3d()\n",
    "pyplot.show()\n",
    "pyplot.close('all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case we start with a spherical initial surface, i.e., $\\Gamma(0)=R_0\\;S^2$, the solution\n",
    "to the mean curvature flow equation is easy to compute:\n",
    "\\begin{align}\n",
    "  \\Gamma(t) &= R(t)\\;S^2 \\\\\n",
    "  R(t) &= \\sqrt{R_0^2-4t}\n",
    "\\end{align}\n",
    "We can use this to check that our implementation is correct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute an averaged radius of the surface\n",
    "def calcRadius(surface):\n",
    "    # compute R = int_x |x| / int_x 1\n",
    "    R   = 0\n",
    "    vol = 0\n",
    "    for e in surface.elements:\n",
    "        rule = geometry.quadratureRule(e.type, 4)\n",
    "        for p in rule:\n",
    "            geo = e.geometry\n",
    "            weight = geo.volume * p.weight\n",
    "            R   += geo.toGlobal(p.position).two_norm * weight\n",
    "            vol += weight\n",
    "    return R/vol"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "endTime = 0.1\n",
    "dt      = 0.02\n",
    "\n",
    "import numpy as np\n",
    "pyplot.figure()\n",
    "pyplot.gca().set_xlim([0,endTime])\n",
    "pyplot.gca().set_ylabel(\"error\")\n",
    "pyplot.gca().set_xlabel(\"time\")\n",
    "\n",
    "numberOfLoops = 2\n",
    "errors = np.zeros(numberOfLoops)\n",
    "totalIterations = np.zeros(numberOfLoops, np.dtype(np.uint32))\n",
    "gridSizes = np.zeros(numberOfLoops, np.dtype(np.uint32))\n",
    "for i in range(numberOfLoops):\n",
    "    positions.interpolate(lambda x: x *  (R0/x.two_norm))\n",
    "    solution.interpolate(lambda x: x)\n",
    "    t       = 0.\n",
    "    R      = calcRadius( surface )\n",
    "    Rexact = math.sqrt(R0*R0-4.*t)\n",
    "    x = np.array([t])\n",
    "    y = np.array([R-Rexact])\n",
    "    model.setConstant(tau,dt)\n",
    "    iterations = 0\n",
    "    while t < endTime:\n",
    "        solution,info = scheme.solve(target=solution)\n",
    "        # move the surface\n",
    "        positions.dofVector.assign(solution.dofVector)\n",
    "        # store some information about the solution process\n",
    "        iterations += int( info[\"linear_iterations\"] )\n",
    "        t          += dt\n",
    "        R      = calcRadius( surface )\n",
    "        Rexact = math.sqrt(R0*R0-4.*t)\n",
    "        # print(\"R_h=\",R, \"Rexact=\",Rexact, \"difference=\",R-Rexact)\n",
    "        x = np.append(x,[t])\n",
    "        y = np.append(y,[R-Rexact])\n",
    "        pyplot.plot(x,y)\n",
    "        display.clear_output(wait=True)\n",
    "        display.display(pyplot.gcf())\n",
    "    errors[i] = abs(R-Rexact)\n",
    "    totalIterations[i] = iterations\n",
    "    gridSizes[i] = grid.size(2)\n",
    "    if i<numberOfLoops-1:\n",
    "        grid.hierarchicalGrid.globalRefine(1)\n",
    "        dt /= 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eocs = np.log(errors[0:][:numberOfLoops-1] / errors[1:]) / math.log(2.)\n",
    "print(eocs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    import pandas as pd\n",
    "    keys = {'size': gridSizes, 'error': errors, \"eoc\": np.insert(eocs,0,None), 'iterations': totalIterations}\n",
    "    table = pd.DataFrame(keys, index=range(numberOfLoops),columns=['size','error','eoc','iterations'])\n",
    "    print(table)\n",
    "except ImportError:\n",
    "    print(\"pandas could not be used to show table with results\")\n",
    "    pass"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
